
import UIKit

protocol GameControllerDelegate {

    func gameControllerCardsDidMatch(firstCard : CardCollectionViewCell, secondCard : CardCollectionViewCell)
    func gameControllerCardsDidNotMatch(firstCard : CardCollectionViewCell, secondCard : CardCollectionViewCell)
    func gameControllerDidEnd()
}

class GameController {

    var firstCard : CardCollectionViewCell? = nil
    var delegate : GameControllerDelegate?
    
    var remainingCards : Int
    let totalNumberOfCards : Int!
    
    
    init(numberOfCards : Int) {
        
        self.totalNumberOfCards = numberOfCards
        self.remainingCards = numberOfCards
    }
    
    
    func checkIsAMatch(cell : CardCollectionViewCell) {
    
        guard isFirstCard() == false else {
            firstCard = cell
            return
        }
        
        if (firstCard?.card?.identifier == cell.card?.identifier) {
           
            delegate?.gameControllerCardsDidMatch(firstCard: firstCard!,
                                                  secondCard: cell)
            
            remainingCards -= 2
            checkIfGameDidEnd(remainingCards: remainingCards)
            
        } else {
        
            delegate?.gameControllerCardsDidNotMatch(firstCard: firstCard!,
                                                     secondCard: cell)
        }
        firstCard = nil
    }
    
    
    internal func checkIfGameDidEnd(remainingCards : Int) {
    
        guard remainingCards == 0 else { return }
        
        self.remainingCards = totalNumberOfCards
        delegate?.gameControllerDidEnd()
    }
    
    
    internal func isFirstCard() -> Bool {
        return firstCard == nil
    }
}
