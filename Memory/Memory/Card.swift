
import UIKit

struct Card {

    var artworkUrl: String?
    var identifier: Double?
}

extension Card {

    init(dictionary : [String : Any]) {
        
        guard let artworkUrl = (dictionary["artwork_url"] as? String),
              let identifier = dictionary["id"] as? Double else { return }
        
        self.artworkUrl = artworkUrl
        self.identifier = identifier
    }
}
