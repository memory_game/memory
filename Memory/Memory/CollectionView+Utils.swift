
import UIKit

extension UICollectionView {

    func dequeueReusableCell<T : UICollectionViewCell>(forIndexPath indexPath: IndexPath) -> T where T : ReusableView {
    
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
        
            fatalError("Could not dequeue cell with identifier: \(T.reuseIdentifier)")
        }
        return cell
    }
}
