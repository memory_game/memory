
import Foundation

enum Result<T, ErrorType> {

    case success(T)
    case failure(ErrorType)
}
