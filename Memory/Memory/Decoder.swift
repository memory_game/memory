
import Foundation

final class Decoder : NSObject {
    
    func decodeData(_ data: Data) -> Result<Any, ErrorType> {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data,
                                                        options: .allowFragments)
            return .success(json)
        } catch {
            return .failure(.parse)
        }
    }
}
