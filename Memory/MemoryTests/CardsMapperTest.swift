
import XCTest

class CardsMapperTest: XCTestCase {
    
    var mapper : CardsMapper?
    let json = Deserializer().deserializeJSON(name: "APIResponse") as! [String : Any]
    
    override func setUp() {
        super.setUp()

        mapper = CardsMapper.init(with: json,
                                  dimension: (1,2))
    
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        mapper = nil
    }

    func testArrayToObjectsFunctionReturnsArrayOfCards() {
    
        let array = json["tracks"] as! [[String : Any]]
        let result =  (mapper!.mapArrayToObjects(array: array,
                                                 uniqueCards: 1)) as [Card]
        XCTAssertTrue(result.count > 0 , "Should return array of cards")
    }
    
    func testArrayOfCardsFunctionReturnsFailureIfDeckDimensionExceedsTracks() {
    
        let mapper = CardsMapper.init(with: json,
                                        dimension: (4,4))
        switch mapper.arrayOfCards() {
        case .failure(_):
            XCTAssertTrue(true, "Should return 'failure'")
        default:
            XCTFail("Should return 'failure'")
        }
    }
}
