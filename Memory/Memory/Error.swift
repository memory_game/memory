
import Foundation

enum ErrorType : Error {

    case network(String?)
    case parse
    case unknown
}

enum ErrorDescription : String {

    case parseErrorDescription = "Some error occurred parsing data."
    case unknownErrorDescription = "Some error occurred. Make sure the quantity of cards requested is not too big and the total number of cards is even"

}

extension ErrorType {


    var description : ErrorDescription {
    
        switch self {
        
        case .parse:
            return ErrorDescription.parseErrorDescription
        case .unknown:
            return ErrorDescription.unknownErrorDescription
        default:
            return ErrorDescription.unknownErrorDescription
        }
    }
}
