
import Foundation

final class Deserializer : NSObject {

    func deserializeJSON(name : String) -> Any? {
    
        let path = Bundle.main.path(forResource: name,
                                    ofType: "json")
        let url = URL(fileURLWithPath: path!)
        
        let jsonData = try? Data(contentsOf: url)
        let jsonResult = try! JSONSerialization.jsonObject(with: jsonData!,
                                                           options: .mutableContainers)
        
        return jsonResult
    }
}
