
import Foundation

protocol Requestable {
    func makeRequest(_ resource: Resource, completion: @escaping(Result<Any?, ErrorType>) -> Void)
}

final class WebService {

    fileprivate let session: URLSession
    
    init(session: URLSession = URLSession(configuration: .default)) {
        self.session = session
    }
}

extension WebService: Requestable {

    func makeRequest(_ resource: Resource, completion: @escaping (Result<Any?, ErrorType>) -> Void) {
        
        let request = resource.request()
        
        session.dataTask(with: request) { (data, response, error) in
            
            switch(data, error) {
            case (let data?, _):
                completion(.success(data))
            case (_, let error?):
                completion(.failure(.network(error.localizedDescription)))
            default: break
            }
        }.resume()
    }
}
