
import Foundation

final class CardsMapper  {

    let json : [String : Any]
    let deckDimension : DeckDimension
    
    init(with json : [String : Any], dimension : DeckDimension) {
        self.json = json
        self.deckDimension = dimension
    }
    
    func arrayOfCards() -> Result<[Card], ErrorType> {
        
        
        guard let tracks = (json["tracks"] as? [[String : Any]]),
                  tracks.count >= uniqueCardsCount(deckDimension: self.deckDimension)  else {
                return .failure(.unknown)
        }
        
        let cards = mapArrayToObjects(array: tracks,
                                             uniqueCards: uniqueCardsCount(deckDimension: deckDimension))
        return .success(cards)
    }
    
    
    internal func mapArrayToObjects(array: [[String : Any]],
                                       uniqueCards: Int) -> [Card]  {
        
        var tracks = array.prefix(uniqueCards).flatMap({(dictionary) in
            return Card.init(dictionary: dictionary)
        })
        
        tracks.append(contentsOf: tracks)
        return tracks
    }
    
    
    // MARK: Helper
    internal func uniqueCardsCount(deckDimension : DeckDimension) -> Int {
        
        let totalCards = deckDimension.width * deckDimension.height
        let uniqueCardsCount = totalCards / 2
        
        return uniqueCardsCount
    }

}

