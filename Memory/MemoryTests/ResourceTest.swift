
import XCTest
@testable import Memory

class ResourceTest: XCTestCase {
    
    var resource : Resource?
    
    override func setUp() {
        super.setUp()

        resource = Resource(endPoint: "79670980?client_id=\(clientID)", method: .GET)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        resource = nil
    }
    
    func testFinalURLIsNotNil() {
    
        XCTAssertNotNil(resource?.finalURL, "URL should not be nil")
    }

    func testResourceReturnsRequest() {
    
        XCTAssertNotNil(resource?.request(), "Should return a URLRequest")
    }
}
