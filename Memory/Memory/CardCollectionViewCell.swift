
import UIKit

class CardCollectionViewCell: UICollectionViewCell, ReusableView, ImageDownloadable {

    @IBOutlet weak var imageView: UIImageView!
    
    var isFliped : Bool = false
    var frontImage : UIImage?
    var card : Card?
    
    
    var backgroundImage : UIImage {
        return UIImage.init(named: "card_background")!
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageView.image = backgroundImage
    }
    
    
    func setupView(with card: Card) {
    
        self.card = card
        
        guard let urlString = card.artworkUrl else { return }
        
        loadImage(urlString: urlString) { [unowned self] (image) in

            DispatchQueue.main.async {
                self.frontImage = image
            }
        }
    }
    
    
    func rotateUp(completion:(() -> Void)?) {
        
        guard isFliped == false else { return }
        
        UIView.transition(with: contentView, duration: 0.5,
                          options: .transitionFlipFromLeft,
                          animations: { [unowned self] in
            
            self.imageView.image = self.frontImage
                            
        }) { (finished) in
            self.isFliped = finished
            completion?()
        }
    }
    
    
    func rotateDown() {
        
        guard isFliped == true else { return }
        
        UIView.transition(with: contentView, duration: 0.5,
                          options: .transitionFlipFromRight,
                          animations: { [unowned self] in
                            
            self.imageView.image = self.backgroundImage
                            
        }) { (finished) in
            self.isFliped = false
        }
    }
}
