
import UIKit

class LoadingView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .clear
        
        let activityIndicator = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
        activityIndicator.startAnimating()
        activityIndicator.center = center
        
        let loadingLabel = UILabel.init(frame: CGRect.init(x: 0,
                                                           y: activityIndicator.frame.origin.y + 10,
                                                           width: frame.width,
                                                           height: 50))
        loadingLabel.textColor = .white
        loadingLabel.font = UIFont.systemFont(ofSize: 16)
        loadingLabel.textAlignment = .center
        loadingLabel.text = "Loading cards"
        
        addSubview(activityIndicator)
        addSubview(loadingLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
