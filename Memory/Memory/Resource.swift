
import Foundation

struct Resource {

    var endPoint: String
    let method: Method
    
    var finalURL : URL {
        
        let urlString = appBaseURL + endPoint
        return URL.init(string: urlString)!
    }
}


extension Resource {
    
    public func request() -> URLRequest {
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        return request
    }
    
    public func request(url : URL) -> URLRequest {
        
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        return request
    }
}
