
import UIKit

extension UIAlertController {

    public class func presentAlert(on parent : UIViewController,
                             title : String = "Memory Game",
                             message : String,
                             actions : [UIAlertAction],
                             cancelTitle : String = "Cancel") {
    
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(cancelAction(parent,
                                     cancelTitle: "Cancel"))
        
        actions.forEach { (action) in
            alert.addAction(action)
        }
    
        parent.present(alert,
                       animated: true,
                       completion: nil)
    }
    

    fileprivate class func cancelAction(_ parent : UIViewController,
                                  cancelTitle : String) -> UIAlertAction {
    
        let action = UIAlertAction.init(title: cancelTitle,
                                        style: .cancel) { (action) in
            parent.dismiss(animated: true, completion: nil)
        }
    
        return action
    }


}
