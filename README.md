# README #

### What is this repository for? ###

This repository contains the source code for the SoundCloud Code Challenge.

### How do I get set up? ###

* Clone the Repository.
* Make sure you have Xcode 8 installed.
* Open "Memory.xcodeproj" in the root folder.
* Run Project.