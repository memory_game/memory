
import UIKit

protocol ReusableView : class {}
protocol ImageDownloadable : class {}

extension ReusableView where Self : UIView {

    static var reuseIdentifier : String {
        return String(describing: self)
    }
}



extension ImageDownloadable {

    func loadImage(urlString : String, completion: @escaping (UIImage) -> Void) {
    
        let emptyImage = UIImage()
        
        guard let url = URL(string: urlString) else {
            completion(emptyImage)
            return
        }
    
        let request = URLRequest(url: url)
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: request) { (data, _, error) in
            
            guard error == nil, data != nil else { return }
            
            let image = UIImage(data: data!)
            completion(image ?? emptyImage)
        }.resume()
    }
}
