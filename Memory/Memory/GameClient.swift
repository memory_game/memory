
import Foundation

typealias DeckDimension = (width:Int, height:Int)

protocol Gettable {
    
    associatedtype ResponseData
    
    func fetchCards(deckDimension : DeckDimension,
                    completion : @escaping (Result<ResponseData, ErrorType>) -> Void)
}

struct GameClient: Gettable {
    
    var cards : [Card] = []
    
    func fetchCards(deckDimension : DeckDimension,
                    completion : @escaping (Result<[Card], ErrorType>) -> Void) {
        
        
        guard totalNumberOfCardsIsEven(deckDimension: deckDimension) else {
            completion(.failure(.unknown))
            return
        }
        
        let resource = Resource(endPoint: "79670980?client_id=\(clientID)", method: .GET)
        
        WebService().makeRequest(resource) { (result) in
            
            switch result {
            case .success(let data):
                
                let decodedData : Result<Any, ErrorType> = Decoder().decodeData(data as! Data)
              
                switch decodedData {
                case .success(let json):
                    
                    let mapper = CardsMapper(with: json as! [String : Any],
                                                   dimension: deckDimension)
                    completion(mapper.arrayOfCards())
                    
                case .failure(let error):
                    completion(.failure(error))
                }
                
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    private func totalNumberOfCardsIsEven(deckDimension: DeckDimension) -> Bool {
    
        return deckDimension.width * deckDimension.height % 2 == 0
    }
}

