
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var client : GameClient? = GameClient()
    let deckDimension : DeckDimension = (4, 4)
    var controller : GameController?
    var loadingView : LoadingView!
    
    var cards : [Card] = [] {
        didSet {
            
            DispatchQueue.main.async {
                self.loadingView.removeFromSuperview()
                self.collectionView.reloadData()
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showLoadingView()
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return .lightContent
    }
    
    
    fileprivate func showLoadingView() {
        
        loadingView = LoadingView(frame: CGRect(origin: .zero,
                                                  size: CGSize(width: 200,
                                                              height: 300)))
        loadingView.center = view.center
        view.addSubview(loadingView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        fetchCards(client: client!)
    }
    
    
    func fetchCards<Client : Gettable>(client : Client) where Client.ResponseData == [Card]  {
        
        client.fetchCards(deckDimension: deckDimension) { [unowned self] (result) in
            
            switch result {
            case .success(var cards):
                
                self.controller = GameController(numberOfCards: cards.count)
                self.controller?.delegate = self
                
                cards.shuffle()
                self.cards = cards
                
            case .failure(let error):
                self.showAlert(message: error.description.rawValue)
            }
        }
    }
    
    
    fileprivate func showAlert(message: String) {
        
        let retryAction = UIAlertAction.init(title: "Try again",
                                             style: .default) { [unowned self] (action) in
                                                
                                                self.fetchCards(client: self.client!)
        }
        
        UIAlertController.presentAlert(on: self, message: message, actions: [retryAction])
        
    }
    
    deinit {
        controller?.delegate = nil
        controller = nil
        client = nil
    }
}



extension ViewController : GameControllerDelegate {
    
    
    func gameControllerCardsDidMatch(firstCard: CardCollectionViewCell,
                                     secondCard: CardCollectionViewCell) {
        
        firstCard.isUserInteractionEnabled = false
        secondCard.isUserInteractionEnabled = false
    }
    
    
    func gameControllerCardsDidNotMatch(firstCard: CardCollectionViewCell,
                                        secondCard: CardCollectionViewCell) {
        
        firstCard.rotateDown()
        secondCard.rotateDown()
    }
    
    
    func gameControllerDidEnd() {
        
        let playAgainAction = UIAlertAction.init(title: "Play again",
                                                 style: .default) { [unowned self] (action) in
                                                    self.turnDownAllCards()
        }
        
        UIAlertController.presentAlert(on: self,
                                       message: "You won!!",
                                       actions: [playAgainAction])
    }
    
    
    func turnDownAllCards() {
        
        for row in 0..<collectionView!.numberOfItems(inSection: 0) {
            
            let cell = collectionView.cellForItem(at: IndexPath(row: row,
                                                                section: 0)) as? CardCollectionViewCell
            cell?.rotateDown()
            cell?.isUserInteractionEnabled = true
        }
        
        cards.shuffle()
        collectionView.reloadData()
    }
}



extension ViewController : UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CardCollectionViewCell
        
        cell.rotateUp() { [unowned self] in
            self.controller?.checkIsAMatch(cell: cell)
        }
    }
}



extension ViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(forIndexPath: indexPath) as  CardCollectionViewCell
        
        let card = cards[indexPath.row]
        cell.setupView(with: card)
        return cell
    }
}



extension ViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let squareSize = screenSize.width / CGFloat(deckDimension.width + 1)
        
        return CGSize.init(width: squareSize, height: squareSize)
    }
}

