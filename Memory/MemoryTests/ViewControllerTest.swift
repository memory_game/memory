
import XCTest
@testable import Memory


class Fake_GameClient : Gettable {
    
    
    var getWasCalled = false
    let result = Result<[Card], ErrorType>.success([Card(dictionary: [:])])
    
    public func fetchCards(deckDimension: DeckDimension,
                    completion: @escaping (Result<[Card], ErrorType>) -> Void) {

        
        self.getWasCalled = true
        completion(result)
    }
}


class ViewControllerTest: XCTestCase {
    
    var viewController : ViewController?
    
    
    override func setUp() {
        super.setUp()
    
        viewController = ViewController()
    }
    
    override func tearDown() {
        super.tearDown()
        
        viewController = nil
    }
    
    func testFetchCardsIsCalled() {
        
        let client = Fake_GameClient()
        
        viewController?.fetchCards(client: client)
        XCTAssertTrue(client.getWasCalled)
    }
}
