
import XCTest

class MockGameController : GameController {

    var isAMatch : Bool = false
    var gameDidEnd : Bool = false
    
    internal override func checkIsAMatch(cell: CardCollectionViewCell) {
        
        if cell.card?.identifier == firstCard?.card?.identifier {
            isAMatch = true
        }
    }
    
    internal override func checkIfGameDidEnd(remainingCards: Int) {
        
        gameDidEnd = remainingCards == 0
    }

}


class GameControllerTest: XCTestCase {
    
    var gameController : MockGameController?
    
    override func setUp() {
        super.setUp()
        
        gameController = MockGameController(numberOfCards: 16)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        gameController = nil
    }
    
    func testIfIsNotAMatch() {
        
        let firstCell = CardCollectionViewCell()
        firstCell.card = Card(artworkUrl : "url1", identifier: 1)
        
        gameController?.checkIsAMatch(cell: firstCell)
        XCTAssertTrue(gameController?.isAMatch == false, "Should not be a match")
    }
    
    func testIfIsAMatch() {
        
        let firstCard = CardCollectionViewCell()
        firstCard.card = Card(artworkUrl : "url1", identifier: 1)
        
        gameController?.firstCard = firstCard
        
        let secondCard = CardCollectionViewCell()
        secondCard.card = Card(artworkUrl : "url1", identifier: 1)

        gameController?.checkIsAMatch(cell: firstCard)
        XCTAssertTrue(gameController?.isAMatch == true, "Should be a match")
    }
    
    func testGameDidEnd() {
    
        gameController?.checkIfGameDidEnd(remainingCards: 4)
        XCTAssertFalse(gameController!.gameDidEnd, "Game did not end")
    
        
        gameController?.checkIfGameDidEnd(remainingCards: 0)
        XCTAssertTrue(gameController!.gameDidEnd, "Game did end")
    }
    
}
